# Gitlab for Yunohost

- [Yunohost project](https://yunohost.org)
- [gitlab website](https://gitlab.com)

Gitlab is a git server similar to github.

**Upgrade this package:**  
`sudo yunohost app upgrade --verbose gitlab -u https://github.com/YunoHost-Apps/gitlab_ynh`

**Multi-user:** Yes with LDAP support.
